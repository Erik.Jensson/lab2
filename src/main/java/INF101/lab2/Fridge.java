package INF101.lab2;

import java.util.ArrayList;

import java.util.List;
import java.util.NoSuchElementException;



public class Fridge implements IFridge {
	
	int max_size = 20;
	ArrayList<FridgeItem> fitems;
	
	public Fridge() {
		this.fitems = new ArrayList<FridgeItem>();
		this.max_size = 20;
		
	}

	
	@Override
	public int nItemsInFridge() {
		// TODO Auto-generated method stub
		return fitems.size();
	}

	@Override
	public int totalSize() {
		// TODO Auto-generated method stub
		return max_size;
	}

	@Override
	public boolean placeIn(FridgeItem item) {
		// TODO Auto-generated method stub
		if(fitems.size() < max_size) {
			fitems.add(item);
			return true;
		}	
		return false;
	}

	@Override 
	public void takeOut(FridgeItem item) {
		// TODO Auto-generated method stub
		if (fitems.contains(item))
			for (FridgeItem elem : fitems) {
				if (elem.equals(item)) {
					fitems.remove(elem);
					break;
				}	
			}
		 else 
			throw new NoSuchElementException();  
		
	}

	@Override
	public void emptyFridge() {
		// TODO Auto-generated method stub
		fitems.removeAll(fitems);
		
		
	}

	@Override
	public List<FridgeItem> removeExpiredFood() {
		ArrayList<FridgeItem> goodFood = new ArrayList<>();
		for (FridgeItem item : fitems) {
			if (item.hasExpired() != true) {
				goodFood.add(item);
				
			}
		}
		fitems.removeAll(goodFood);
		return fitems;
	}

}













